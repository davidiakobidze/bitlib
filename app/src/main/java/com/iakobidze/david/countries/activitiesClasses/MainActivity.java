package com.iakobidze.david.countries.activitiesClasses;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.iakobidze.david.countries.R;

public class MainActivity extends AppCompatActivity {

    public Button button1;
    public Button button2;
    public Button button3;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



//პირველი ღილაკი
            button1=(Button)findViewById(R.id.button1);
            button1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openActivity1();
                }
            });
//პირველი ღილაკი
// მეორე ღილაკი
        button2=(Button)findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity2();
            }
        });
//მეორე ღილაკი

        //მესამე ღილაკი
        button3=(Button)findViewById(R.id.button3);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openActivity3();
            }
        });
//მესამე ღილაკი




    }


    //პირველი ღილაკი
    public void openActivity1(){
        Intent intent=new Intent(this,OnlineBooks.class);
        startActivity(intent);
    }
    //პირველი ღილაკი


    //მეორე ღილაკი
    public void openActivity2(){
        Intent intent2=new Intent(this,books_activity.class);
        startActivity(intent2);
    }
    //მეორე ღილაკი

    //მესამე ღილაკი
    public void openActivity3(){
        Intent intent3=new Intent(this,MapsActivity.class);
        startActivity(intent3);
    }
    //მესამე ღილაკი



}


