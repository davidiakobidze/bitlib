package com.iakobidze.david.countries.activitiesClasses;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.iakobidze.david.countries.R;
import com.iakobidze.david.countries.adapters.MyRecyclerAdapter;
import com.iakobidze.david.countries.data.BookData;

import java.util.ArrayList;
import java.util.List;

public class books_activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.books_activity);


        RecyclerView recyclerView=findViewById(R.id.scorllableView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);


        List<String> listData=new ArrayList<>();
        int count=0;
        for (int i = 0; i< BookData.bookName.length; i++){

            listData.add(BookData.bookName[count]+"\n\n"+BookData.bookAuthor[count]);
            count ++;
            if(count==BookData.bookName.length){
                count=0;
            }
        }
        MyRecyclerAdapter adapter =new MyRecyclerAdapter(listData);
        recyclerView.setAdapter(adapter);
    }
}
