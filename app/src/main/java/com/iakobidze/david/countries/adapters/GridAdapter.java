package com.iakobidze.david.countries.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.iakobidze.david.countries.R;

/**
 * Created by david on 12/25/2017.
 */

public class GridAdapter extends BaseAdapter {

    private int icons[];
    private String letters[];
    private Context context;

private LayoutInflater layoutInflater;


    public GridAdapter(Context context,int icons[],String letters[]){
        this.context=context;
        this.icons=icons;
        this.letters=letters;

    }



    @Override
    public int getCount() {
        return letters.length;
    }

    @Override
    public Object getItem(int i) {
        return letters[i];
    }

    @Override
    public long getItemId(int i) {


        return i;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        View gridView=convertView;


        if (convertView==null){
            layoutInflater=(LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            gridView=layoutInflater.inflate(R.layout.layout_grid,null);


        }
        ImageView icon=(ImageView)gridView.findViewById(R.id.icon);
        TextView letter=(TextView) gridView.findViewById(R.id.letters);

        icon.setImageResource(icons[i]);

        letter.setText(letters[i]);


        return gridView;
    }
}
