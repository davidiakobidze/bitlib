package com.iakobidze.david.countries.activitiesClasses;

import android.support.v4.app.FragmentActivity;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.iakobidze.david.countries.R;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;


        mMap.addMarker(new MarkerOptions().position(new LatLng(41.729945, 44.786851)).title("Biblus Tsereteli"));
        mMap.addMarker(new MarkerOptions().position(new LatLng(41.691890, 44.870511)).title("Biblus Varketili"));
        mMap.addMarker(new MarkerOptions().position(new LatLng(41.690744, 44.899689)).title("Biblus East Point"));
        mMap.addMarker(new MarkerOptions().position(new LatLng(41.683280, 44.840913)).title("Biblus Isani"));
        mMap.addMarker(new MarkerOptions().position(new LatLng(41.681220, 44.823833)).title("Biblus Old Tbilisi"));
        mMap.addMarker(new MarkerOptions().position(new LatLng(41.701945, 44.793223)).title("Biblus Rustaveli"));
        mMap.addMarker(new MarkerOptions().position(new LatLng(41.709632, 44.785444)).title("Biblus Kostava"));
        mMap.addMarker(new MarkerOptions().position(new LatLng(41.725281, 44.742200)).title("Biblus Vazha Pshavela"));
        mMap.addMarker(new MarkerOptions().position(new LatLng(41.725856, 44.751816)).title("Libra Vazha Pshavela"));
        mMap.addMarker(new MarkerOptions().position(new LatLng(41.726527, 44.771292)).title("Santa Esperanza Iosebidze"));
        mMap.addMarker(new MarkerOptions().position(new LatLng(41.697487, 44.801926)).title("Bona Causa"));
        mMap.addMarker(new MarkerOptions().position(new LatLng(41.789223, 44.765427)).title("Libra King Parnavaz Aveniu"));


        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(41.715071, 44.828661),13));


    }
}
