package com.iakobidze.david.countries.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.iakobidze.david.countries.R;

import java.util.List;

/**
 * Created by david on 12/24/2017.
 */



public class MyRecyclerAdapter extends RecyclerView.Adapter<MyRecyclerAdapter.MyViewHolder> {

    private List<String> listData;



    public MyRecyclerAdapter(List<String> listData){
        this.listData=listData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

      View view=  LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerlist_item, parent, false);
        MyViewHolder viewHolder =new MyViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
    holder.listItemNameView.setText(listData.get(position));

    }

    @Override
    public int getItemCount() {
        return listData ==null ? 0 : listData.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
TextView listItemNameView;

        public MyViewHolder(View itemView) {
            super(itemView);
            listItemNameView=(TextView) itemView.findViewById(R.id.listitem_name);

        }
    }
}
