package com.iakobidze.david.countries.activitiesClasses;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.iakobidze.david.countries.R;
import com.iakobidze.david.countries.adapters.GridAdapter;

public class OnlineBooks extends AppCompatActivity {

    GridView gridView;

   String []letterList={
            "სამოსელი პირველი","სამი მუშკეტერი","პროცესი","ქარიშხლიანი უღელტეხილი","99 ფრანკი","ძმები კარამაზოვები"};
    int lettersIcon[]={R.drawable.samoseli,
            R.drawable.samimushketeri,
            R.drawable.procesi,
            R.drawable.qu,
            R.drawable.franck,
            R.drawable.karamazovebi
    };

    String [] links={
            //სამოსელი პირველი
            "http://litklubi.ge/biblioteka/admin/files/სამოსელი%20პირველი.pdf",
            //სამი მუშკეტერი
            "https://docs.google.com/file/d/0BwCMyTECcJPnOU1NVmJxZGc4ZDg/edit",
            //პროცესი
            "https://burusi.files.wordpress.com/2017/06/kafka.pdf",
            //ქარიშხლიანი უღელტეხილი
            "https://drive.google.com/file/d/0B9b-lCigRttwVHpDRGRIQkNVZ1E/view",
            //99 ფრანკი
            "https://www.scribd.com/document/326261936/99-ფრანკი-pdf",
            //ძმები კარამაზოვები
            "http://litklubi.ge/biblioteka/admin/files/ძმები%20კარამაზოვები.pdf"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online_books);


        gridView=(GridView)findViewById(R.id.gridView);


        GridAdapter adapter=new GridAdapter(OnlineBooks.this,lettersIcon,letterList);

        gridView.setAdapter(adapter);


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent intent=new Intent(Intent.ACTION_VIEW, Uri.parse(links[i]));
                startActivity(intent);
            }
        });
    }
}
